# JuanMisc

A quark for SuperCollider with Juan Pampin's classes.

Developed at [DXARTS](https://dxarts.washington.edu/).

## Getting started

Install the quark by running
```supercollider
Quarks.install("https://gitlab.com/dxarts/projects/juanmisc")
```
in SuperCollider.

