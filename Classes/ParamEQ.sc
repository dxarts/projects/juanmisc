ParamEQ {
	*ar {
		arg in, ampDB=#[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ], mul=1, add=0;
		var frq, amp, flt, bands;
		frq = Array.geom(13, 20000, 1.75.reciprocal).reverse;
		bands = frq.keep(12);
		amp = ampDB;
		flt = in;
		bands.do{ arg f, i;
			(i == 0).if({
				flt = BLowShelf.ar(flt, f, 1, amp[i])
			},{
				flt = BPeakEQ.ar(flt, f, 1, amp[i])
			});
		};
		flt = BHiShelf.ar(flt, frq[12], 1, amp[12]);
		^(flt * mul + add)
	}
}
